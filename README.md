# PB Re-Recoding Tester  Ver 2.0.1#

App to aid in the testing of re-recorded PB's for the Natural Numbers Project.
Will also be used to fine tune the per voice profile gaps, the logic to implemented at the stores and the the per voice mastering profiles.

### What is this repository for? ###

* The app (source) for the PB Tester
* Version 2.0.1
	**20150817 - enabled mp3 bitrate via conf


### Requirements###

#### Ruby ver 2.0.0+####

 - Tested under Ruby ver 2.0.0

#### Binaries & other ####
* Install Homebrew => http://brew.sh
* Install SoX 	=> brew install sox
* Install the lame lib 	=> brew install lame

#### To Use This App ####
* Clone this repo to ~/projects/rr_apps/pb_tester_2
* Edit the "file_location" attribute (line 3) in the "profiles.json" file to tell it where the audio files are located. It is located in
the lib > prefs directory.
* Browse to the root (~/projects/rr_pb_tester_2) and run "$ ruby run_app.rb"
   Alternatively for a more verbose output (to view more messages) browse to (~/projects/rr_pb_tester_2/lib/support) and run "$ ruby tester.rb"

#### Expected Directories ####
* 01_Profiles <- contains the dirs per voice with the pb's & values. As per Nat Nums.
* 02_Music <- contains the music files for mixing in under the ads.

### Known Issues ###
* "Relative path potentially not safe" - this is due to the use of a system command to delete unwanted files in the temp working directory. It will be fixed in the next release.
* (Errno::ENOENT) crashes! The app currently crashes with this error. Should this happen just run it again. Usually it works after a crash. This is being debugged.

### Who do I talk to? ###
* dteren@gmail.com
* ZaPOP IT