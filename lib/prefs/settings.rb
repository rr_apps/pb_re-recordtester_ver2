require 'json'
require_relative './../support/helpers'

def path_exp(pwd, rel_path)
  File.expand_path(File.join(pwd, rel_path))
end

PWD                     = File.dirname(__FILE__)
# ROOT                  = path_exp(PWD, './../../')
CONFIGS                 = path_exp(PWD, './../prefs')
RUNNER                  = path_exp(PWD, './../support/tester.rb')
RUBY                    = '/usr/bin/ruby'.freeze

file_path               = File.join(CONFIGS, 'profiles.json')
file                    = File.read(file_path)
data_hash               = JSON.parse(file)
main_settings           = data_hash['main']
main_effect_state       = main_settings['bypass']
BITRATE                 = main_settings['mp3']['bitrate']

SOX                     = '/usr/local/bin/sox'.freeze # Should be replaced with a locator.
SOXI                    = '/usr/local/bin/soxi'.freeze
LAME                    = '/usr/local/bin/lame'.freeze

FILE_LOCATION           = main_settings['file_location']

MASTER_BYPASS           = main_effect_state['master_bypass']
MASTER_PRE_GAIN_BYPASS  = main_effect_state['pre_gain_bypass']
MASTER_FILTER_BYPASS    = main_effect_state['filter_bypass']
MASTER_COMPRESSION      = main_effect_state['compression_bypass']
MASTER_EQ_BYPASS        = main_effect_state['eq_bypass']
MASTER_POST_GAIN        = main_effect_state['post_gain_bypass']

PROFILES                = path_exp(FILE_LOCATION, '01_profiles')
MUSIC                   = path_exp(FILE_LOCATION, '02_music')
OUTPUTS                 = path_exp(FILE_LOCATION, '03_output')
TEMPDIR                 = path_exp(FILE_LOCATION, 'temp')

PB_A                    = File.join(TEMPDIR, 'pb_a.wav')
PB_B                    = File.join(TEMPDIR, 'pb_b.wav')
VAL_A                   = File.join(TEMPDIR, 'vala.wav')
VAL_B                   = File.join(TEMPDIR, 'valb.wav')
VAL_C                   = File.join(TEMPDIR, 'valc.wav')
CURR                    = File.join(TEMPDIR, 'curr.wav')
PADFILE1                = File.join(TEMPDIR, 'pad_1.wav')
PADFILE2                = File.join(TEMPDIR, 'pad_2.wav')
PADFILE3                = File.join(TEMPDIR, 'pad_3.wav')
CURR_PAD                = File.join(TEMPDIR, 'curr_pad.wav')
CONCATAD                = File.join(TEMPDIR, 'concatad.wav')
TRIMUSIC                = File.join(TEMPDIR, 'music_trimmed.wav')
MIXEDAD                 = File.join(TEMPDIR, 'mixed_ad.wav')
