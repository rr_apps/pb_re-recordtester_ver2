require 'fileutils'
require_relative '../../lib/support/audio_processor'
require_relative '../../lib/prefs/settings'

# Remove unwanted characters & words
def to_clean(to_clean)
  # to_remove = ['.', '..', '.DS_Store']
  to_clean.drop('.DS_Store')
end

def dir_list(get_list)
  Dir.entries(get_list)
end

def file_basename(file, ext = '.mp3')
  File.basename(file, ext)
end

def file_pathandname(file)
  File.split(file)
end

def file_dirname(file)
  File.dirname(file)
end

def file_exist(file)
  File.exist?(file)
end

def file_dir_exist(path)
  File.exist?(path)
end

def file_delete(file)
  File.delete(file)
  File.exist?(file)
end
