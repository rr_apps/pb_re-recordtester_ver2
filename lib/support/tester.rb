require 'pp'
require_relative '../../lib/support/file_management'
require_relative '../../lib/prefs/settings'
require_relative '../../lib/support/helpers'
require_relative '../../lib/support/audio_processor'

time          = Time.new
date_stamp    = time.strftime('%Y_%m_%d-%H_%M')

vo_profiles = array_wash(Dir.entries(PROFILES))

vo_profiles.each do |i|
  the_vo, lang  = vo_selector(i)
  vo_path       = File.join(PROFILES, i)
  pb_path       = File.join(vo_path, 'pb')
  vala_path     = File.join(vo_path, 'val_a')
  valb_path     = File.join(vo_path, 'val_b')
  valc_path     = File.join(vo_path, 'val_c')
  curr_path     = File.join(vo_path, 'curr')

  def val_c_check(valc_path)
    return false unless Dir.exist?(valc_path)
    return false if (Dir.entries(valc_path) - %w( . .. )).empty?
    true
  end

  def curr_check(curr_path)
    return false unless Dir.exist?(curr_path)
    return true if (Dir.entries(curr_path) - %w( . .. )).empty?
    true
  end

  val_c_exists = val_c_check(valc_path)
  curr_exists  = curr_check(curr_path)

  the_pbs       = compile_list(pb_path)
  the_valas     = compile_list(vala_path)
  the_valbs     = compile_list(valb_path)
  the_valcs     = compile_list(valc_path) if val_c_exists
  the_currs     = compile_list(curr_path) if curr_exists

  curr_count = 0
  the_pbs.each do |pb|
    random_vala = the_valas[rand(the_valas.length)]
    random_valb = the_valbs[rand(the_valbs.length)]

    ad_pb    = File.join(File.expand_path(pb_path), pb)
    ad_vala  = File.join(vala_path, random_vala)
    ad_valb  = File.join(valb_path, random_valb)

    ad_valc = File.join(valc_path, random_valb) if val_c_exists

    if curr_exists

      random_curr = the_currs[curr_count]
      curr_count += 1
      curr_count = 0 if curr_count == the_currs.size
      ad_curr = File.join(curr_path, random_curr)
    end

    vo = "#{the_vo}_#{lang}"
    sp = 'sp' + pb.sub(/^pb/, '')

    ad_final = File.join(File.expand_path(OUTPUTS), i, date_stamp)
    ad_sp    = File.join(ad_final, sp)

    puts "###################### New Advert ########################"

    msg = "#{Time.now}:[D]:Ad Specs: pb: #{path_to_basename(ad_pb)} : ad_vala: #{path_to_basename(ad_vala)} : ad_valb: #{path_to_basename(ad_valb)} : ad_valc: #{path_to_basename(ad_valc)} : ad_curr: #{path_to_basename(ad_curr)} : vo: #{vo.downcase} :ad_sp: #{path_to_basename(ad_sp)}"
    puts msg

    advert = AudioProc.new(ad_pb, ad_vala, ad_valb, ad_valc, ad_curr, vo.downcase, ad_sp)
    advert.launch!
  end
end
