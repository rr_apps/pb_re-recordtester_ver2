require 'fileutils'
require_relative '../../lib/prefs/settings'
require_relative './helpers'
require_relative './voice_configs'
require_relative './echolog'

class AudioProc < VoiceConfigs
  def initialize(pb, val_a, val_b, val_c = '', curr = '', vo, ad_sp)
    @settings = VoiceConfigs.new(vo)
    @logger = Echolog.new
    @pb     = pb
    @val_a  = val_a
    @val_b  = val_b
    @val_c  = val_c
    @curr   = curr
    @vo     = vo
    @ad_sp  = ad_sp
  end

  def launch!
    # remove the temp dir
    FileUtils.rm_rf(TEMPDIR) while File.exist?(TEMPDIR)
    # check if temp dir exists if not create it
    temp_dir
    # Split the PB by detecting silences & convert to wav
    split_pb
    # Convert vals a & b to wav & trim silence from end
    trim_ends(@val_a, VAL_A)
    trim_ends(@val_b, VAL_B)
    trim_ends(@val_c, VAL_C) unless @val_c.nil?
    trim_ends(@curr,  CURR) unless @curr.nil?

    # Create the pad files
    create_pad(PADFILE1, @settings.gaps[0])
    create_pad(PADFILE2, @settings.gaps[1])
    create_pad(PADFILE3, @settings.gaps[2])
    curr_gap = 0
    curr_gap = @settings.curr_gaps(@curr) unless @curr.nil?
    create_pad(CURR_PAD, curr_gap)

    # Create the advert
    concat
    # Get concatenated advert length
    duration
    get_music
    trim_music
    # Mix the music in under the advert & master
    mastering
    mix_and_master
    # Bounce out the final ad in the required format
    @new_file = File.join(TEMPDIR, File.basename(@ad_sp))
    wav_mp3

    FileUtils.mkdir_p(File.dirname(@ad_sp))
    # File.rename(MIXEDAD,  @ad_sp)
    # FileUtils.mv(@new_file, @ad_sp)
    @logger.echolog(-> { FileUtils.mv(@new_file, @ad_sp) }, "Move to Compiled Ad Directory: #{path_end(@ad_sp)} ")

    # remove the temp dir
    FileUtils.rm_rf(TEMPDIR) if File.exist?(TEMPDIR)
  end

  def temp_dir
    @logger.echolog(-> { `mkdir #{TEMPDIR}` unless File.exist?(TEMPDIR) }, 'Make /tempdir if needed')
  end

  def wav_mp3
    cmd = "#{LAME} -S --silent -b #{BITRATE} -h #{MIXEDAD} #{@new_file}"
    @logger.echolog(-> { `#{cmd}` }, "Convert to MP3: #{path_end(@new_file, 2)}")
  end

  def split_pb
    @logger.echolog(-> { `#{SOX} #{@pb} #{TEMPDIR}/part.wav silence 1 0.00 -48d 1 4.0 0.1% : newfile : restart` }, "Split the PB at silences: #{path_end(@pb)} ")

    @logger.echolog(-> { delete_small_files(TEMPDIR, 55) }, 'Delete unwanted file from split process')
    @logger.echolog(-> { trim_ends("#{TEMPDIR}/part001.wav", PB_A); trim_ends("#{TEMPDIR}/part002.wav", PB_B); true }, "Trim the PB parts & rename #{path_end(PB_A, 2)} & #{path_end(PB_B, 2)} ")
    @logger.echolog(-> { FileUtils.rm(File.join(TEMPDIR, 'part001.wav')); FileUtils.rm(File.join(TEMPDIR, 'part002.wav')) }, 'Remove the part001 & part002 wav files')
  end

  def create_pad(pad_file, gap)
    cmd = %(#{SOX} -n -b 16 -c 2 -r 44100 "#{pad_file}" trim 0.0 "#{gap}" reverse fade t 100s reverse)
    @logger.echolog(-> { `#{cmd}` }, "Create pad with gap: #{gap}")
  end

  def trim_ends(in_file, out_file)
    @logger.echolog(-> { `#{SOX} #{in_file} #{out_file} silence 1 0.01 -60d reverse silence 1 0.01 -60d reverse pad 0 0 fade t 100s reverse fade t 100s reverse` }, "Trim ends on: #{path_end(in_file, 1)}")
  end

  def path_end(in_file, depth = 3)
    return '' if in_file.nil?
    in_file.split('/')[-depth..-1].join('/')
  end

  def fade(in_file, out_file)
    `SOX #{in_file} #{out_file} fade t 100s reverse fade t 100s reverse`
  end

  def duration
    @dur = `"#{SOXI}" -D "#{CONCATAD}"`.to_f.round
  end

  def get_music
    z = Dir.glob("#{MUSIC}/*")
    @music = z[rand(z.length)]
  end

  def mastering
    pregain = ''; comp = ''; equal = ''; filt = ''; postgain = ''
    pregain     = @settings.pre_gain unless @settings.pre_gain_bypass
    comp        = @settings.compressor unless @settings.compression_bypass
    equal       = @settings.equalizer unless @settings.eq_bypass
    filt        = @settings.filter unless @settings.filter_bypass
    postgain    = @settings.post_gain unless @settings.post_gain_bypass
    @mastering  = "#{pregain} #{comp} #{equal} #{filt} #{postgain}" unless @settings.master_bypass
  end

  def concat
    round_robin_b = VAL_C
    round_robin_b = VAL_B unless @val_c == ''
    currency = CURR
    currency = '' if @curr.nil?
    cmd = "#{SOX} #{PB_A} #{PADFILE1} #{VAL_A} #{CURR_PAD} #{currency} #{PADFILE2} #{VAL_B} #{PADFILE3} #{PB_B} #{PADFILE1} #{VAL_A} #{CURR_PAD} #{currency} #{PADFILE2} #{round_robin_b} -c 2 #{CONCATAD} pad 2.5 2.5"
    @logger.echolog(-> { `#{cmd}` }, "Create concatenated advert: #{path_end(CONCATAD, 2)}")
  end

  def trim_music
    @logger.echolog(-> { `"#{SOX}" -v -1.0 "#{@music}" "#{TRIMUSIC}" trim 0 #{@dur} gain 0 ` }, "Trim music length: #{@dur} : #{path_end(@music, 2)}")
  end

  def mix_and_master
    cmd = "#{SOX} -m #{CONCATAD} #{TRIMUSIC} #{MIXEDAD} #{@mastering} fade h 0:2 0 0:3"

    @logger.echolog(-> { system(cmd.to_s) }, "Mix music under advert & master: #{path_end(MIXEDAD, 2)}")
  end
end
