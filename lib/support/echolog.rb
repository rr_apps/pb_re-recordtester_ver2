
class Echolog
  def echolog(cmd, msg)
    res = cmd.call
    aok = res ? "OK!\n" : "FAIL!\n"
    puts "#{Time.now}:[D]:#{msg}: #{aok}"
  end
end
