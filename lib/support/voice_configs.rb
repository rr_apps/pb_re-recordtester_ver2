require 'json'
require_relative './../prefs/settings'
require_relative './helpers'

class VoiceConfigs
  def initialize(vo)
    @vo = vo
    file_path = File.join(CONFIGS, 'profiles.json')
    file = File.read(file_path)
    @data_hash = JSON.parse(file)
  end

  def gaps
    @data_hash[@vo]['gaps']
  end

  def curr_gaps(curr)
    curr = path_to_basename(curr).delete('.mp3').to_i
    @data_hash[@vo]['currency'][curr]
  end

  def master_bypass
    @data_hash[@vo]['bypass']['master_bypass']
  end

  def pre_gain_bypass
    @data_hash[@vo]['bypass']['pre_gain_bypass']
  end

  def filter_bypass
    @data_hash[@vo]['bypass']['pre_gain_bypass']
  end

  def compression_bypass
    @data_hash[@vo]['bypass']['compression_bypass']
  end

  def eq_bypass
    @data_hash[@vo]['bypass']['pre_gain_bypass']
  end

  def post_gain_bypass
    @data_hash[@vo]['bypass']['eq_bypass']
  end

  def pre_gain
    @data_hash[@vo]['pre_gain']
  end

  def filter
    @data_hash[@vo]['filter']
  end

  def compressor
    data = @data_hash[@vo]['compression']
    comp = 'compand '
    data.each do |i|
      comp << i
    end
    comp
  end

  def equalizer
    data = @data_hash[@vo]['equalizer']
    eq_l = ''
    data.each do |i|
      eq_l << "equalizer #{i} "
    end
    eq_l
  end

  def post_gain
    @data_hash[@vo]['post_gain']
  end
end
