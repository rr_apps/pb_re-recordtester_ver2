require 'pp'

module Helpers
  def array_wash(the_list)
    the_list - %w(. .. .DS_Store)
  end

  def compile_list(pb_path)
    array_wash(Dir.entries(File.expand_path(pb_path)))
  end

  def absolute_paths_of_dir_contents(dir, list = '*')
    path = File.join(dir, list)
    Dir.glob(path) - %w(. .. .DS_Store)
  end

  def delete_small_files(path, size = 50)
    absolute_paths_of_dir_contents(path).each do |file|
      FileUtils.rm(file) if FileTest.size(file) < size
    end
  end

  def path_to_basename(path)
    return 'none' if path.nil?
    File.basename(path)
  end

  def echo_split(disp, splt = ' /')
    puts "\n" + '-' * 40 + "\n"
    disp.split(splt).each do |i|
      puts i
    end
    puts '-' * 40 + "\n"
  end

  # VO selector & language parser.
  def vo_selector(voice)
    lang = 'Eng' if voice.downcase.include?('eng')
    lang = 'Afr' if voice.downcase.include?('afr')
    the_vo = voice[0..-5]
    voice = the_vo.capitalize
    [voice, lang]
  end

  def get_path(vo_path, type = '', rnd = false) # type = pb, val_a, val_b or music / true = random
    the_path = File.join(vo_path, type) # set path to the dir
    the_path = File.expand_path(the_path) # expand the path to dir
    the_files = Dir.entries(the_path) # get list of available files
    array_wash(the_files) # purge the file list
    if rnd
      rnd_file = the_files[rand(the_files.length)] # get a random val_a
      File.join(the_path, rnd_file)
    else
      return the_path, the_files
 end
 end
end

include Helpers
